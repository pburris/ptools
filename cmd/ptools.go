package main

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/jumballaya/ptools"
	"github.com/jumballaya/ptools/racer"
)

func RacerAction(args ptools.CommandArgs, flags ptools.FlagValues) (string, error) {
	flagLen := len(flags)

	if flagLen == 0 {
		fmt.Println(args)
		u1 := args[0]
		u2 := args[1]
		return racer.Race(u1, u2)
	}

	returnString := ""

	for name, value := range flags {
		if name == "verbose" {
			returnString += "\n\nVERBOSE OUTPUT FOR RACER:\n"
			returnString += "========================="
			u1 := args[0]
			u2 := args[1]
			res, err := racer.Race(u1, u2)
			if err != nil {
				return "", err
			}
			returnString += fmt.Sprintf("\n\nTHE WINNER IS:\n%s !!!!", res)
			return returnString, nil
		}

		if name == "rounds" {
			returnString += "\n\nROUND AFTER ROUND OUTPUT FOR RACER:\n"
			returnString += "===================================\n\n"
			u1 := args[0]
			u2 := args[1]

			rounds, err := strconv.Atoi(value)
			if err != nil {
				return "", err
			}

			for i := 0; i < rounds; i++ {
				res, err := racer.Race(u1, u2)
				if err != nil {
					return "", err
				}
				returnString += fmt.Sprintf("ROUND %d WINNER IS: %s \n", i+1, res)
			}
			return returnString, nil
		}
	}
	return "DID NOT RUN", ptools.NewError("the application did not run")
}

func SetupRacer(a *ptools.App) {
	racerCmd := &ptools.Command{
		Name:        "racer",
		Usage:       "ptools racer www.example.com www.example.org",
		Description: "Racer takes 2 urls and races them to see which returns a response first",
	}

	flags := []ptools.Flag{
		ptools.Flag{
			Name:  "rounds",
			Usage: "How many rounds to run",
		},
		ptools.Flag{
			Name:  "verbose",
			Usage: "Verbose printing of the race",
		},
	}

	racerCmd.Flags = flags
	a.RegisterCommand(racerCmd, RacerAction)
}

func main() {
	args := os.Args
	app := ptools.NewApp()
	app.Name = "ptools"
	app.Usage = "ptools COMMAND"
	app.Description = "The kitchen sink toolchain you never wanted."
	app.Version = "1.0.0"

	SetupRacer(app)

	res, err := app.Run(args)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(res)

}
