# Ptools!
##### The kitchen sink toolchain you never wanted

#### Inspiration:
* [Learn Go with tests](https://quii.gitbook.io/learn-go-with-tests/go-fundamentals/select)
* [Carl M. Johnson's post "More Than a Dozen Command Line Tools I've Written-and So Can You!"](https://blog.carlmjohnson.net/post/2018/go-cli-tools/)

#### Installation
`go get -u github.com/jumballaya/ptools`

`cd $GOPATH/src/github.com/jumballaya/ptools`

`go install cmd/ptools.go`


#### Commands
```
Usage: ptools COMMAND

The kitchen sink toolchain you never wanted.

Options:
    racer       Race two urls to see which one returns a response the fastest

    help        This help screen
```

#### Wishlist

* Prettifier
  - JSON
  - HTML
  - CSS
  - JS
* Image/Video conversion
* Password generator
* Password storage
* File/folder encrypt/decrypt manager for things like my .aws folder or .ssh folder (decrypts before use, but stays encrypted until then, something like that?)
* Email/text notifier with a timer
* HTTP header examiner
* Web scraper/searching tool for
  - Stack Overflow
  - Reddit
* HTTP Honeypot (similar to Carl M. Johnson's from the post above)
* Generate random tweet when given 2 twitter users to analyze and 'meld' together
