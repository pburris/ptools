package racer

import (
	"fmt"
	"net/http"
	"time"
)

// Race races 2 urls using default settings
func Race(a, b string) (winner string, err error) {
	timeout := 10 * time.Second
	return ConfigurableRace(a, b, timeout)
}

// ConfigurableRace is the fully configurable version of the race
func ConfigurableRace(a, b string, timeout time.Duration) (winner string, err error) {
	select {
	case res := <-ping(a):
		if res != false {
			return a, nil
		}
		return "NO WINNER!", nil
	case res := <-ping(b):
		if res != false {
			return b, nil
		}
		return "NO WINNER!", nil
	case <-time.After(timeout):
		return "", fmt.Errorf("timed out waiting for '%s' and '%s'", a, b)
	}
}

func ping(url string) chan bool {
	ch := make(chan bool)
	go func() {
		res, err := http.Get(url)

		if err != nil {
			ch <- false
			return
		}

		if res.StatusCode < 200 || res.StatusCode > 399 {
			ch <- false
			return
		}

		ch <- true
	}()
	return ch
}
