package racer

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestRacer(t *testing.T) {
	t.Run("returns the winner of an http GET race", func(t *testing.T) {
		slowServer := makeDelayedServer(20 * time.Millisecond)
		fastServer := makeDelayedServer(0 * time.Millisecond)

		defer fastServer.Close()
		defer slowServer.Close()

		want := fastServer.URL
		got, _ := Race(slowServer.URL, fastServer.URL)

		if got != want {
			t.Errorf("\nGot: '%s'\nWant: '%s'", got, want)
		}
	})

	t.Run("returns an error if a server doesn't respond within the configured time", func(t *testing.T) {
		serverA := makeDelayedServer(5 * time.Second)
		serverB := makeDelayedServer(6 * time.Second)

		defer serverA.Close()
		defer serverB.Close()

		_, err := ConfigurableRace(serverA.URL, serverB.URL, 4*time.Second)

		if err == nil {
			t.Error("expected an error but didn't get one")
		}
	})

	t.Run("returns no winner statement if the status code is not 2xx or 3xx", func(t *testing.T) {
		serverA := makeCustomStatusServer(http.StatusNotFound)
		serverB := makeCustomStatusServer(http.StatusInternalServerError)

		defer serverA.Close()
		defer serverB.Close()

		got, _ := Race(serverA.URL, serverB.URL)
		want := "NO WINNER!"

		if got != want {
			t.Errorf("\nGot: '%s'\nWant: '%s'", got, want)
		}
	})

	t.Run("automatically adds http:// infront of the url if the protocol is not present", func(t *testing.T) {
		urlA := "www.google.com"
		urlB := "https://www.google.com"

		_, err := Race(urlA, urlB)

		if err != nil {
			t.Errorf("Failed to format url correctly")
		}
	})
}

func BenchmarkRacer(b *testing.B) {
	serverA := makeDelayedServer(0 * time.Millisecond)
	serverB := makeDelayedServer(0 * time.Millisecond)

	defer serverA.Close()
	defer serverB.Close()

	for i := 0; i < b.N; i++ {
		Race(serverA.URL, serverB.URL)
	}
}

func makeDelayedServer(delay time.Duration) *httptest.Server {
	handler := func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(delay)
		w.WriteHeader(http.StatusOK)
	}
	return httptest.NewServer(http.HandlerFunc(handler))
}

func makeCustomStatusServer(status int) *httptest.Server {
	handler := func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(0 * time.Second)
		w.WriteHeader(status)
	}
	return httptest.NewServer(http.HandlerFunc(handler))
}
