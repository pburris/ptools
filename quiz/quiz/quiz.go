package quiz

import (
	"bufio"
	"bytes"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"time"
)

type item struct {
	question string `json:"question"`
	answer   string `json:"answer"`
}

type Quiz struct {
	questions []item        `json:"questions"`
	score     int           `json:"score"`
	timer     time.Duration `json:"timer"`
}

func (q *Quiz) String() string {
	var out bytes.Buffer

	fmt.Println(fmt.Sprintf("Quiz is timed for %d seconds", q.timer))
	for i, qu := range q.questions {
		text := fmt.Sprintf("%d. Question: %s, Answer: %s\n", i+1, qu.question, qu.answer)
		out.WriteString(text)
	}
	return out.String()
}

func (q *Quiz) Start() {
	timesUp := false
	timerChan := time.NewTimer(q.timer).C
	answerChan := make(chan string)

quizloop:
	for i, quest := range q.questions {

		fmt.Println(fmt.Sprintf("Question number %d:", i+1))
		fmt.Println(quest.question)
		fmt.Print("> ")

		go func() {
			var answer string
			fmt.Scanf("%s\n", &answer)
			answerChan <- answer
		}()

		select {
		case <-timerChan:
			fmt.Println(fmt.Sprintf("\nTimes up! Your score is: %d out of %d", q.score, len(q.questions)))
			timesUp = true
			break quizloop
		case answer := <-answerChan:
			if answer == quest.answer {
				q.score += 1
			}
			fmt.Println("")
		}
	}

	if !timesUp {
		fmt.Println(fmt.Sprintf("You completed the quiz! Your score is: %d out of %d", q.score, len(q.questions)))
	}
}

func New(file string, time time.Duration) *Quiz {
	csvFile, err := os.Open(file)
	if err != nil {
		log.Fatal(err)
	}

	reader := csv.NewReader(bufio.NewReader(csvFile))
	var questions []item

	for {
		line, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			log.Fatal(err)
		}

		questions = append(questions, item{
			question: line[0],
			answer:   line[1],
		})
	}

	return &Quiz{
		questions: questions,
		score:     0,
		timer:     time,
	}
}
