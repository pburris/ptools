package ptools

import (
	"flag"
	"fmt"
)

// Flag holds the information for a flag, or an option, for the api
// example: command --flag
type Flag struct {
	Name  string
	Usage string
}

// @TODO: Need to figure out flags next and then refactor Command.Run() to differentiate
// between flags and commands. I also need to edit the menus to reflect flags.

// RunFn is the type of function that runs a command
type RunFn func(CommandArgs, FlagValues) (string, error)

// CommandArgs is the slice of commands
type CommandArgs []string

// FlagValues is the map of flag values for a given cli call
type FlagValues map[string]string

// Command is struct that contains information about the individual commands of the CLI application
// An example would be 'build' in 'go build'
type Command struct {
	Name        string
	Usage       string
	Flags       []Flag
	FlagSet     *flag.FlagSet
	Description string
	SubCommands []*Command
	action      RunFn
}

// Run passes the incoming args into the user created action RunFn and return the results
// first the Run function checks for defaults like the description menu
func (c *Command) Run(args CommandArgs) (string, error) {
	c.initFlagSet()
	c.FlagSet.Parse(args)

	flagValues := make(map[string]string)

	c.FlagSet.Visit(func(fl *flag.Flag) {
		flagValues[fl.Name] = fl.Value.String()
	})

	argLen := len(args)
	if argLen != 0 && args[0] == "help" {
		return c.Menu(), nil
	}
	if argLen > 0 {
		for i := 0; i < len(args); i++ {
			for j := 0; j < len(c.SubCommands); j++ {
				arg := args[i]
				cmd := c.SubCommands[j]
				if arg == cmd.Name {
					return cmd.Run(args[i:])
				}
			}
		}
	}

	return c.action(c.FlagSet.Args(), flagValues)
}

// Menu returns the created menu for the command
func (c *Command) Menu() string {
	menu := fmt.Sprintf("Usage: %s\n%s", c.Usage, c.Description)
	menu = fmt.Sprintf("%s\n\nOptions:\n", menu)
	if len(c.SubCommands) > 0 {
		for _, cmd := range c.SubCommands {
			menu = fmt.Sprintf("%s    %s        %s\n", menu, cmd.Name, cmd.Description)
		}
	}

	menu = fmt.Sprintf("%s    help        This help screen\n", menu)
	return menu
}

// @TODO: Abstract this into a Flags builder for the Command struct's Flags field
// It has to take in a command struct and set its FlagSet
func (c *Command) initFlagSet() {
	fs := flag.NewFlagSet(c.Name, flag.ExitOnError)
	for _, fl := range c.Flags {
		fs.String(fl.Name, "", fl.Usage)
	}

	c.FlagSet = fs
}

// App is the entire CLI application
type App struct {
	Name        string
	Usage       string
	Version     string
	Description string
	Commands    []*Command
}

// NewApp creates a new CLI app
func NewApp() *App {
	return &App{}
}

// RegisterCommand adds commands to an application
func (a *App) RegisterCommand(cmd *Command, fn RunFn) error {
	cmd.action = fn
	a.Commands = append(a.Commands, cmd)
	return nil
}

// Menu displays the application's help menu
func (a *App) Menu() string {
	menu := fmt.Sprintf("Usage: %s\n%s", a.Usage, a.Description)
	menu = fmt.Sprintf("%s\n\nOptions:\n", menu)
	if len(a.Commands) > 0 {
		for _, cmd := range a.Commands {
			menu = fmt.Sprintf("%s    %s        %s\n", menu, cmd.Name, cmd.Description)
		}
	}

	menu = fmt.Sprintf("%s    help        This help screen\n", menu)
	return menu
}

// Run starts the application and is given a slice of cli commands and options
func (a *App) Run(args CommandArgs) (string, error) {
	argLen := len(args)

	if (argLen == 0) || (argLen == 1 && args[0] == "help") {
		return a.Menu(), nil
	}
	if argLen == 1 && args[0] == "--version" {
		return fmt.Sprintf("v%s", a.Version), nil
	}

	if argLen > 0 {
		for i := 0; i < len(args); i++ {
			for j := 0; j < len(a.Commands); j++ {
				arg := args[i]
				cmd := a.Commands[j]
				if arg == cmd.Name {
					return cmd.Run(args[i+1:])
				}
			}
		}
	}

	return a.Menu(), nil
}
