package ptools

import (
	"fmt"
)

type URLError struct {
	url string
}

func (e URLError) Error() string {
	return fmt.Sprintf("url %s is not formatted correctly", e.url)
}

type GenericError struct {
	s string
}

func (e GenericError) Error() string {
	return e.s
}

func NewError(text string) error {
	return &GenericError{text}
}
