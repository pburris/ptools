// +build mage

package main

import (
	"fmt"
	"os"
	"os/exec"

	"github.com/magefile/mage/mg"
)

// Build is the step that requires additional params, or platform specific steps for example
func Build() error {
	mg.Deps(InstallDeps)
	fmt.Println("Building...")
	cmd := exec.Command("go", "build", "-o", "dist/ptools", "cmd/ptools.go")
	return cmd.Run()
}

// Install moves the built binary to the user's bin folder
func Install() error {
	mg.Deps(Build)
	fmt.Println("Installing...")
	return os.Rename("./dist/ptools", "/usr/bin/ptools")
}

// InstallDeps is where vendor dependecies are installed
func InstallDeps() error {
	fmt.Println("Installing Deps...")
	cmd := exec.Command("echo", "no deps...")
	return cmd.Run()
}

// Clean up after yourself
func Clean() {
	fmt.Println("Cleaning...")
	os.RemoveAll("dist/ptools")
}
