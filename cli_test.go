package ptools

import (
	"fmt"
	"testing"
)

func TestCLI(t *testing.T) {

	app := NewApp()
	app.Name = "app"
	app.Usage = "app COMMAND"
	app.Description = "Testing my CLI"
	app.Version = "1.0.0"

	t.Run("Should be able to create app and print help menu", func(t *testing.T) {
		want := `Usage: app COMMAND
Testing my CLI

Options:
    help        This help screen
`
		got, _ := app.Run([]string{"help"})

		if want != got {
			t.Errorf("\nGot: '%s'\nWant: '%s'", got, want)
		}
	})

	t.Run("returns help menu if you type help or no command", func(t *testing.T) {
		noParam, _ := app.Run([]string{""})
		withHelp, _ := app.Run([]string{"help"})

		if withHelp != noParam {
			t.Errorf("\nNo Parameter: '%s'\nWith help Parameter: '%s'", noParam, withHelp)
		}
	})

	t.Run("return the version if the --version flag is used", func(t *testing.T) {
		want := "v1.0.0"
		got, _ := app.Run([]string{"--version"})

		if want != got {
			t.Errorf("\nGot: '%s'\nWant: '%s'", got, want)
		}
	})
}

func TestApp(t *testing.T) {
	app := NewApp()
	app.Name = "app"
	app.Usage = "app COMMAND"
	app.Description = "Testing my CLI"
	app.Version = "1.0.0"

	t.Run("should be able to add a command and see it in the help menu", func(t *testing.T) {
		cmd := &Command{Name: "racer", Description: "Race two urls against each other"}
		action := func(args CommandArgs, flags FlagValues) (string, error) {
			return "ran!", nil
		}
		app.RegisterCommand(cmd, action)
		want := `Usage: app COMMAND
Testing my CLI

Options:
    racer        Race two urls against each other
    help        This help screen
`
		got, _ := app.Run([]string{"help"})

		if want != got {
			t.Errorf("\nGot: '%s'\nWant: '%s'", got, want)
		}
	})

	t.Run("should be able to add a command and then execute it", func(t *testing.T) {
		action := func(args CommandArgs, flags FlagValues) (string, error) {
			return "ran!", nil
		}
		calcCommand := &Command{
			Name:        "calc",
			Usage:       fmt.Sprintf("%s calc COMMAND", app.Name),
			Description: "Calculates numbers",
		}
		app.RegisterCommand(calcCommand, action)

		want := "ran!"
		got, _ := app.Run([]string{"calc"})

		if got != want {
			t.Errorf("\nGot: '%s'\nWant: '%s'", got, want)
		}
	})

	printAction := func(args CommandArgs, flags FlagValues) (string, error) {
		ret := ""

		for fl := range flags {
			ret += fl + " "
		}

		return ret, nil
	}
	printCommand := &Command{
		Name:        "print",
		Usage:       fmt.Sprintf("%s print COMMAND", app.Name),
		Description: "Prints stuff",
		Flags: []Flag{
			Flag{
				Name:  "flaga",
				Usage: "how to use this flag",
			},
			Flag{
				Name:  "flagb",
				Usage: "how to use this flag",
			},
			Flag{
				Name:  "flagc",
				Usage: "how to use this flag",
			},
			Flag{
				Name:  "flagd",
				Usage: "how to use this flag",
			},
		},
	}

	app.RegisterCommand(printCommand, printAction)

	flagTests := []struct {
		name    string
		command []string
		want    string
	}{
		{name: "basic, one", command: []string{"print", "--flaga=a"}, want: "flaga "},
		{name: "basic, two", command: []string{"print", "--flaga=a", "--flagb=b"}, want: "flaga flagb "},
		{name: "basic, three", command: []string{"print", "--flaga=a", "--flagb=b", "--flagc=c"}, want: "flaga flagb flagc "},
		{name: "basic, four", command: []string{"print", "--flaga=a", "--flagb=b", "--flagc=c", "--flagd=d"}, want: "flaga flagb flagc flagd "},
	}

	for _, tt := range flagTests {
		t.Run(tt.name, func(t *testing.T) {
			got, _ := app.Run(tt.command)

			if got != tt.want {
				t.Errorf("\nGot: '%s'\nWant: '%s'", got, tt.want)
			}
		})
	}
}

func TestCommand(t *testing.T) {
	app := NewApp()
	app.Name = "app"
	app.Usage = "app COMMAND"
	app.Description = "Testing my CLI"
	app.Version = "1.0.0"

	calcCommand := &Command{
		Name:        "calc",
		Usage:       fmt.Sprintf("%s calc COMMAND", app.Name),
		Description: "Calculates numbers",
	}
	action := func(args CommandArgs, flags FlagValues) (string, error) {
		return "ran!", nil
	}

	app.RegisterCommand(calcCommand, action)

	t.Run("return command's menu output with help", func(t *testing.T) {
		got, _ := app.Run([]string{"calc", "help"})
		want := `Usage: app calc COMMAND
Calculates numbers

Options:
    help        This help screen
`

		if got != want {
			t.Errorf("\nGot: '%s'\nWant: '%s'", got, want)
		}
	})

	t.Run("should be able to add a subcommand to a command execute it", func(t *testing.T) {
		t.Errorf("New failing test")
	})
}
